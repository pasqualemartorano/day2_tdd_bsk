package tdd.training.bsk;

import java.util.ArrayList;

public class Game {
	protected ArrayList<Frame> frames;
	protected int firstBonusThrow = 0;
	protected int secondBonusThrow = 0;
	/**
	 * It initializes an empty bowling game.
	 * @throws BowlingException 
	 */
	public Game() throws BowlingException {
		frames = new ArrayList<Frame>();
		addFrame(new Frame(10, 0));
		addFrame(new Frame(10, 0));
		addFrame(new Frame(10, 0));
		addFrame(new Frame(10, 0));
		addFrame(new Frame(10, 0));
		addFrame(new Frame(10, 0));
		addFrame(new Frame(10, 0));
		addFrame(new Frame(10, 0));
		addFrame(new Frame(10, 0));
		addFrame(new Frame(10, 0));
		
		if(frames.size()!=10)
			throw new BowlingException("Error creating the game");
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {
		frames.add(frame);
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) throws BowlingException {
		if(index<0 || index >9)
			throw new BowlingException("Index error");
		return frames.get(index);
	}

	/**
	 * It return the frames of this game.
	 */
	public ArrayList<Frame> getFrames(){
		return frames;
	}
	
	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		this.firstBonusThrow = firstBonusThrow;
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		this.secondBonusThrow = secondBonusThrow;
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		return firstBonusThrow;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		return secondBonusThrow;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException {
		int score = 0;
		
		if(getFrameAt(9).isSpare())
			setFirstBonusThrow(7);
		if(getFrameAt(9).isStrike()) {
			setFirstBonusThrow(10);
			setSecondBonusThrow(10);
		}
		
		for(int i=0; i<=9; i++) {
			Frame f = getFrameAt(i);
			if(f.isSpare() && i<9) {
				f.setBonus(getFrameAt(i+1).getFirstThrow());
			}
			if(f.isStrike() && i<9) {
				if(getFrameAt(i+1).isStrike()) {
					if(i == 8)
						f.setBonus(getFrameAt(i+1).getFirstThrow() + getFirstBonusThrow());
					else
						f.setBonus(getFrameAt(i+1).getFirstThrow() + getFrameAt(i+2).getFirstThrow());
				}
				else f.setBonus(getFrameAt(i+1).getFirstThrow() + getFrameAt(i+1).getSecondThrow());
			}
			score += f.getScore();
		}
		
		score += getFirstBonusThrow() + getSecondBonusThrow();
		return score;	
	}

}
