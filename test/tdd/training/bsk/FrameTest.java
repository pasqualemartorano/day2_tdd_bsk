package tdd.training.bsk;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Test;


public class FrameTest {
	
	@Test
	public void testGetThrows() throws BowlingException {
		//This method tests the user story 1
		int firstThrow = 4;
		int secondThrow = 5;
		Frame f = new Frame (firstThrow, secondThrow);
		assertEquals(firstThrow, f.getFirstThrow());
		assertEquals(secondThrow, f.getSecondThrow());
	}
	
	@Test
	public void testGetScore() throws BowlingException {
		//This method tests the user story 2
		int firstThrow = 4;
		int secondThrow = 5;
		int score = firstThrow + secondThrow;
		Frame f = new Frame (firstThrow, secondThrow);
		assertEquals(score, f.getScore());
	}
	
	@Test
	public void testGameCreation() throws BowlingException {
		//This method tests the first part of user story 3
		int check=0;
		int[] thrws = new int[20];
		thrws[0]=10; 	thrws[1]=0;
		thrws[2]=10; 	thrws[3]=0;
		thrws[4]=10;	thrws[5]=0;
		thrws[6]=10;	thrws[7]=0;
		thrws[8]=10;	thrws[9]=0;
		thrws[10]=10; 	thrws[11]=0;
		thrws[12]=10;	thrws[13]=0;
		thrws[14]=10;	thrws[15]=0;
		thrws[16]=10;	thrws[17]=0;
		thrws[18]=10;	thrws[19]=0;
		Game g = new Game();
		ArrayList<Frame> frames = g.getFrames();
		int[] gotThrws = new int[20];
		int i=0;
		for(Frame f: frames) {
			gotThrws[i]=f.getFirstThrow();
			i++;
			gotThrws[i]=f.getSecondThrow();
			i++;
		}
		if(Arrays.equals(thrws, gotThrws))
			check=1;
		
		assertEquals(1, check);	
	}
	
	@Test
	public void testGetFrameAt() throws BowlingException {
		//This method tests the second part of user story 3
		int index = 4;
		Game g = new Game();
		Frame gotFrame = g.getFrameAt(index);
		int firstThrow = 10;
		int secondThrow = 0;
		int gotFirstThrow = gotFrame.getFirstThrow();
		int gotSecondThrow = gotFrame.getSecondThrow();
		assertEquals(firstThrow, gotFirstThrow);
		assertEquals(secondThrow, gotSecondThrow);
	}
	
	@Test
	public void testGameScore() throws BowlingException {
		//This method tests the game score (used in more user stories)
		int score = 300;
		Game g = new Game();
		int gotScore = g.calculateScore();
		assertEquals(score, gotScore);
	}

	@Test
	public void testGameGetFirstBonusThrow() throws BowlingException {
		//This method tests the method getFirstBonusThrow() (user story 10 onwards)
		int firstBonusThrow = 10;
		Game g = new Game();
		g.calculateScore();
		int gotFBT = g.getFirstBonusThrow();
		assertEquals(firstBonusThrow, gotFBT);
	}
	
	@Test
	public void testGameGetSecondBonusThrow() throws BowlingException {
		//This method tests the method getSecondBonusThrow() (user story 11 onwards)
		int secondBonusThrow = 10;
		Game g = new Game();
		g.calculateScore();
		int gotSBT = g.getSecondBonusThrow();
		assertEquals(secondBonusThrow, gotSBT);
	}
	
}
